class ForbiddenOperation(Exception):
    pass


class RejectedOperation(Exception):
    pass
