from fastapi import FastAPI
from app.db import get_db
from app.routers.appointment import endpoints as endpoints_appointments
from app.routers.user import endpoints as endpoints_users
from app.routers.patient import endpoints as endpoints_patients

import logging

logger = logging.getLogger()

app = FastAPI()

app.include_router(endpoints_appointments.router, prefix="/v1/appointments", tags=["appointments"])
app.include_router(endpoints_users.router, prefix="/v1/users", tags=["users"])
app.include_router(endpoints_patients.router, prefix="/v1/patients", tags=["patients"])


@app.on_event("startup")
async def startup():
    await get_db().connect()
    logger.debug("Connection to DB successful!")


@app.on_event("shutdown")
async def shutdown():
    logger.debug("Shutdown... database disconnects")
    await get_db().disconnect()
