import logging
from databases import Database
from app.config import config
from sqlalchemy import MetaData

_db = None
logger = logging.getLogger()


def get_db():
    global _db
    if not _db:
        _db = init_api_database()
    return _db


def init_api_database():
    db_url = get_api_db_url()
    logger.debug(db_url)
    return Database(db_url)


def get_api_db_url():
    db_name = config.get("database_name")
    db_host = config.get("database_host")
    db_user = config.get("database_user")
    db_password = config.get("database_password")
    return f"postgresql://{db_user}:{db_password}@{db_host}/{db_name}"


metadata = MetaData()
