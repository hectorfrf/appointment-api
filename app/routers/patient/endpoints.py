from fastapi import APIRouter, Response, status, HTTPException, Request
from app.routers.user.models import UserIn, UserOut, UserBase, UserProfile
from app.routers.user.api import create_user, update_user, get_users, get_user
from typing import Dict, Any
from app.db import get_db

router = APIRouter()


@router.get("", response_model=Dict[str, Any])
async def patient_list(
    request: Request,
    response: Response,
    gender: str = None,
    state: str = None
) -> dict:
    try:
        query = get_users(profile=UserProfile.PATIENT, gender=gender, state=state)
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=error,
        )

    response.status_code = status.HTTP_200_OK
    return {"result": await get_db().fetch_all(query)}


@router.get("/{patient_id_number}", response_model=UserOut)
async def get_patient_by_id_number(request: Request, response: Response, patient_id_number: str):
    patient = await get_user(id_number=patient_id_number)
    if not patient:
        raise HTTPException(status_code=404, detail="Patient does not exist")
    response.status_code = status.HTTP_200_OK
    return patient


@router.post("", response_model=UserOut)
async def patient_create(request: Request, response: Response, user: UserIn):

    last_record = None
    try:
        user.profile = UserProfile.PATIENT
        last_record = await create_user(user=user)
    except ValueError as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(error)
        )

    response.status_code = status.HTTP_201_CREATED
    return last_record


@router.patch("/{user_id}", response_model=UserOut)
async def patient_update(request: Request, response: Response, user_id: int, user: UserBase):
    try:
        updated_user = await update_user(id=user_id, user=user)
        if not updated_user:
            raise HTTPException(status_code=404, detail="Patient does not exist")
    except ValueError as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    response.status_code = status.HTTP_200_OK

    return updated_user
