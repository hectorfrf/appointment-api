from fastapi import APIRouter, Response, status, HTTPException, Request
from .models import UserIn, UserOut, UserBase
from .api import create_user, update_user, get_users
from typing import Dict, Any
from app.db import get_db

router = APIRouter()


@router.get("", response_model=Dict[str, Any])
async def user_list(
    request: Request,
    response: Response,
) -> dict:
    try:
        query = get_users()
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=error,
        )

    response.status_code = status.HTTP_200_OK
    return {"result": await get_db().fetch_all(query)}


@router.post("", response_model=UserOut)
async def user_create(request: Request, response: Response, user: UserIn):

    last_record = None
    try:
        last_record = await create_user(user=user)
    except ValueError as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(error)
        )

    response.status_code = status.HTTP_201_CREATED
    return last_record


@router.patch("/{user_id}", response_model=UserOut)
async def user_update(request: Request, response: Response, user_id: int, user: UserBase):
    try:
        updated_user = await update_user(id=user_id, user=user)
    except ValueError as error:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(error))
    response.status_code = status.HTTP_200_OK

    return updated_user
