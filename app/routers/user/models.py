from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    Enum,
    Table,
    Date,
    DateTime,
    ForeignKey
)
from pydantic import BaseModel
from typing import Optional
import enum
from app.db import metadata
from datetime import date, datetime


class UserProfile(enum.IntEnum):
    DOCTOR = 1
    PATIENT = 2


class UserGender(enum.IntEnum):
    MALE = 1
    FEMALE = 2


users = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("id_number", String(100), nullable=False, unique=True),
    Column("name", String(255), nullable=False),
    Column("birth_date", Date, nullable=False),
    Column("phone", String(100), nullable=False),
    Column("email", String(255), nullable=True),
    Column("gender", Enum(UserGender), index=True, nullable=True),
    Column("country", String(255), nullable=False),
    Column("state", String(255), index=True, nullable=False),
    Column("timezone", String(255), nullable=False),
    Column("created_at", DateTime, nullable=False),
    Column("profile", Enum(UserProfile), nullable=False),
    Column("doctor", Integer, ForeignKey("users.id"), nullable=True),
    Column("disabled", Boolean, nullable=False),
)


class UserIn(BaseModel):
    id_number: str
    name: str
    birth_date: date
    phone: str
    email: Optional[str]
    gender: Optional[str]
    country: str
    state: str
    timezone: str
    created_at: datetime = datetime.utcnow()
    profile: UserProfile
    doctor: Optional[int]
    disabled: Optional[bool] = False


class UserOut(BaseModel):
    id: int
    id_number: str
    name: str
    birth_date: date
    phone: str
    email: Optional[str]
    gender: Optional[str]
    country: str
    state: str
    timezone: str
    created_at: datetime
    profile: UserProfile
    doctor: Optional[int]
    disabled: bool


class User(BaseModel):
    id: int
    id_number: str
    name: str
    birth_date: date
    phone: str
    email: Optional[str]
    gender: Optional[str]
    country: str
    state: str
    timezone: str
    created_at: datetime
    profile: UserProfile
    doctor: Optional[int]
    disabled: bool

    def is_doctor(self):
        return self.profile == UserProfile.DOCTOR

    def is_patient(self):
        return self.profile == UserProfile.PATIENT


class UserBase(BaseModel):
    id: Optional[int] = None
    id_number: Optional[str] = None
    name: Optional[str] = None
    birth_date: Optional[date] = None
    phone: Optional[str] = None
    email: Optional[str] = None
    gender: Optional[str] = None
    country: Optional[str] = None
    state: Optional[str] = None
    timezone: Optional[str] = None
    created_at: Optional[datetime] = None
    profile: Optional[UserProfile] = None
    doctor: Optional[int] = None
    disabled: Optional[bool] = None
