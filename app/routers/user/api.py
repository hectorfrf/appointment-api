from .models import UserIn, users, User, UserBase
from typing import Optional
from sqlalchemy import and_, func
from app.db import get_db


def get_users(*, profile=None, gender=None, state=None):
    """Get users according to params.

    A list of patients is obtained.

    Parameters
    ----------
    profile
        User profile (DOCTOR, PATIENT)
    gender
        User Gender (MALE, FEMALE)
    state
        User state

    Returns
    -------
    Users
        Query of users

    """
    query = users.select().where(users.c.disabled == False)
    if profile:
        query = query.where(users.c.profile == profile)
    if gender:
        query = query.where(users.c.gender == gender)
    if state:
        query = query.where(func.lower(users.c.state).contains(state))
    return query


async def create_user(*, user: UserIn) -> User:
    """Create user in DB.

    Create a user record in the database

    Parameters
    ----------
    user
        The user to be created.

    Returns
    -------
    User
        User if successful, None otherwise.

    """
    existing_user = await get_user(id_number=user.id_number)
    if existing_user:
        raise ValueError(f"User with ID number '{user.id_number}' already exists")

    query = users.insert().values(**dict(user))
    id_user = await get_db().execute(query)
    if not id_user:
        return None

    return await get_user(id=id_user)


async def get_user(*, id: int = None, id_number: int = None) -> Optional[User]:
    """Obtein a User in db.

    A User is obtained for any of the incoming parameters.

    Parameters
    ----------
    id
        ID User.
    id_number
        ID Number.

    Returns
    -------
    User
        User if successful, None otherwise.

    """
    query = None
    if id:
        query = users.select().where(users.c.id == id)
    if id_number:
        query = users.select().where(users.c.id_number == id_number)

    result = await get_db().fetch_one(query)
    if not result:
        return None

    return User(**dict(result))


# @db.transaction
async def update_user(*, id: int, user: UserBase) -> User:
    """Update partial a user in db.

    A User is partially or completely updated with his id.

    Parameters
    ----------
    id
        Id User.
    user
        User to update.

    Returns
    -------
    User
        User if successful, None otherwise.

    Raises
    ------
    ValueError
        If password generate error

    """

    dict_user = user.dict(exclude_unset=True)

    query = users.update().where(and_(users.c.id == id)).values(**dict_user)
    await get_db().execute(query)
    return await get_user(id=id)
