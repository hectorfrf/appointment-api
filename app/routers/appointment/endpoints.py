from fastapi import APIRouter, Response, status, HTTPException, Request
from .models import AppointmentIn, AppointmentOut, AppointmentBase
from .api import create_appointment, update_appointment, get_appointment, get_appointments
from typing import Dict, Any
from app.db import get_db
import logging

router = APIRouter()
logger = logging.getLogger()


@router.get("", response_model=Dict[str, Any])
async def appointment_list(
    request: Request,
    response: Response,
    type: str = None,
) -> dict:
    try:
        query = get_appointments(type=type)
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=error,
        )

    response.status_code = status.HTTP_200_OK
    return {"result": await get_db().fetch_all(query)}


@router.post("", response_model=AppointmentOut)
async def appointment_create(request: Request, response: Response, appointment: AppointmentIn):
    last_record = None
    try:
        last_record = await create_appointment(appointment=appointment)
    except ValueError as error:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=str(error))
    response.status_code = status.HTTP_201_CREATED
    return last_record


@router.get("/{appointment_id}", response_model=AppointmentOut)
async def get_appointment_by_type(request: Request, response: Response, appointment_id: int):
    appointment = await get_appointment(id=appointment_id)
    if not appointment:
        raise HTTPException(status_code=404, detail="Appointment does not exist")
    response.status_code = status.HTTP_200_OK
    return appointment


@router.patch("/{appointment_id}")
async def appointment_update(
    response: Response, appointment_id: int, appointment: AppointmentBase
):
    update_record = await update_appointment(id=appointment_id, appointment=appointment)
    if not update_record:
        raise HTTPException(status_code=404, detail="Appointment does not exist")
    response.status_code = status.HTTP_200_OK
    return update_record
