from sqlalchemy import Column, Integer, String, Text, Boolean, Enum, Table, ForeignKey, DateTime
from pydantic import BaseModel
from typing import Optional
from app.routers.user.models import users
import enum
from app.db import metadata
from datetime import datetime


class AppointmentType(enum.IntEnum):
    CALL = 1
    VIDEO_CALL = 2


appointments = Table(
    "appointments",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("patient_age", String(100), nullable=False),
    Column("datetime", DateTime(timezone=True), nullable=False),
    Column("duration", String(100), nullable=False),
    Column("type", Enum(AppointmentType), index=True, nullable=False),
    Column("reason", Text, nullable=False),
    Column("created_at", DateTime(timezone=True), nullable=False),
    Column("deleted", Boolean, default=False, nullable=False),
    Column("doctor", Integer, ForeignKey(users.c.id), nullable=False),
    Column("patient", Integer, ForeignKey(users.c.id), nullable=False),
)


class AppointmentIn(BaseModel):
    datetime: datetime
    type: str
    reason: str
    created_at: datetime = datetime.utcnow()
    deleted: bool = False
    doctor: int
    patient: int


class AppointmentOut(BaseModel):
    id: int
    patient_age: str
    datetime: datetime
    duration: str
    type: str
    reason: str
    created_at: datetime
    doctor: int
    patient: int


class Appointment(BaseModel):
    id: int
    patient_age: str
    datetime: datetime
    duration: str
    type: str
    reason: str
    created_at: datetime
    doctor: int
    patient: int
    deleted: bool


class AppointmentBase(BaseModel):
    patient_age: Optional[str] = None
    datetime: Optional[datetime] = datetime
    duration: Optional[str] = None
    type: Optional[str] = None
    reason: Optional[str] = None
    created_at: Optional[datetime] = None
    doctor: Optional[int] = None
    patient: Optional[int] = None
    deleted: Optional[bool] = None
