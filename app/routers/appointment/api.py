from app.db import get_db, init_api_database
from .models import AppointmentIn, AppointmentBase, appointments, Appointment, AppointmentType
from app.routers.user.api import get_user
from app.routers.user.models import UserProfile
import logging
from sqlalchemy import and_
import pytz
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from sqlalchemy import text

logger = logging.getLogger()


def get_appointments(*, type: str = None):
    """Get appointments according to params.

    A list of appointments is obtained.

    Parameters
    ----------
    type
        Appointment type (CALL, VIDEO_CALL)

    Returns
    -------
    Appointments
        Query of appointments

    """
    type_condition = f"AND ap.type = '{type}'" if type else ""
    query = text(
        f"""
            SELECT ap.id, ap.patient_age,
            ap.datetime at time zone us2.timezone as appointment_datetime,
            ap.duration, ap.type, ap.reason,
            ap.created_at at time zone us2.timezone as appointment_create_date,
            us.name as doctor_name, us2.name as patient_name
            FROM appointments ap
            JOIN users us ON us.id = ap.doctor
            JOIN users us2 ON us2.id = ap.patient
            WHERE ap.deleted = 'false'
            {type_condition};
        """
    )
    return query


async def create_appointment(*, appointment: AppointmentIn) -> Appointment:
    """Create a appointment in db.

    Assign the values in the query to create them in the db.

    Parameters
    ----------
    appointment
        Appointment to created.

    Returns
    -------
    Appointment
        A appointment if successful, None otherwise.

    """
    patient = await get_user(id=appointment.patient)
    today = date.today()
    age = relativedelta(today, patient.birth_date)
    if appointment.type == AppointmentType.CALL.name:
        appointment_duration = str(15)
    else:
        appointment_duration = str(30)

    query = appointments.insert().values(
        patient_age=str(age.years),
        datetime=appointment.datetime.astimezone(pytz.timezone('UTC')),
        duration=appointment_duration,
        type=appointment.type,
        reason=appointment.reason,
        deleted=appointment.deleted,
        created_at=appointment.datetime.astimezone(pytz.timezone('UTC')),
        doctor=appointment.doctor,
        patient=appointment.patient,
    )

    id_created = await get_db().execute(query)
    if not id_created:
        return None

    return await get_appointment(id=id_created)


async def get_appointment(*, id: int = None) -> Appointment:
    """Obtein a appointment in db.

    A appointment is obtained for any of the incoming parameters.

    Parameters
    ----------
    id
        Id Appointment

    Returns
    -------
    Appointment
        Appointment if successful, None otherwise.

    """
    query = appointments.select().where(appointments.c.deleted == False)
    if id:
        query = query.where(appointments.c.id == id)

    result = await get_db().fetch_one(query)
    if not result:
        return None
    return Appointment(**dict(result))


async def update_appointment(*, id: int, appointment: AppointmentBase) -> Appointment:
    """Update partial a appointment in db.

    A appointment is partially or completely updated with his id.

    Parameters
    ----------
    id
        Id Appointment.
    appointment
        Appointment to update.

    Returns
    -------
    Appointment
        Appointment if successful, None otherwise.

    """
    dict_appointment = appointment.dict(exclude_unset=True)

    query = (
        appointments.update()
        .where(and_(appointments.c.id == id, appointments.c.deleted == False))
        .values(**dict_appointment)
    )
    await get_db().execute(query)
    return await get_appointment(id=id)
