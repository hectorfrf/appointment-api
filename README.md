# Appointment-API

- Create virtual environment:
`mkvirtualenv -p /usr/bin/python3.6 appointment-api`

- Install dependencies:
`pip install -r requirements.txt`

- Edit database info:
`vim config.yaml`

- Create DB:
`CREATE DATABASE appointment_api
 ENCODING 'UTF8'
 LC_COLLATE='C'
 LC_CTYPE='C'
 template=template0
 OWNER postgres;`

- Create all tables according to alembic migrations:
`alembic upgrade head`

- Run:
`uvicorn app.main:app --reload`


----------//-----------


- Json to create patient:
`
{
    "id_number": "1234567838",
    "name": "Hector Ramirez",
    "birth_date": "1990-07-22",
    "phone": "408 123 9863",
    "gender": "MALE",
    "country": "USA",
    "state": "Florida",
    "timezone": "US/Pacific",
    "profile": 2,
    "doctor": 1
}
`


- Json to create an appointment:
`
{
    'datetime': '2021-06-28 9:00',
    'type': 'CALL',
    'reason': 'Testing',
    'doctor': 1,
    'patient': 5,
}
{
    'datetime': '2021-07-01 15:00',
    'type': 'VIDEO_CALL',
    'reason': 'Testing 2',
    'doctor': 1,
    'patient': 3
}
`